#ifndef MUSHROOMWAR_ASSETMANAGER_H
#define MUSHROOMWAR_ASSETMANAGER_H

#include "../../Include.h"
#include "GameLoop.h"

class AssetManager
{
public:
	/**
	 * Load an image from the asset/img directory
	 * @return The surface of the image
	 */
	static SDL_Surface* LoadImage(std::string);

	/**
	 * Load an image from the asset/img directory
	 * @param renderer the renderer where attach the surface
	 * @return The texture of the image
	 */
	static SDL_Texture* LoadImage(std::string, SDL_Renderer* renderer);

	/**
	 * Load a font from the asset/font directory
	 * @param fontSize the font size
	 * @return The font
	 */
	static TTF_Font* LoadFont(std::string, int fontSize);
};

#endif
