#ifndef MUSHROOMWAR_GAMEOBJECT_H
#define MUSHROOMWAR_GAMEOBJECT_H

#include "../../Include.h"
#include "Scene.h"
#include "GameLoop.h"

class Scene;

/**
* Class generic for all gameObject who need update in game
*/
class GameObject
{
protected:
	bool active = true;

	Scene *scene = nullptr; /// The scene assigned to the object
	SDL_Renderer *renderer = nullptr; /// The renderer, get from game loop
	SDL_Rect *rectTransform = new SDL_Rect(); /// The rect of the object

public:
	virtual ~GameObject();
	SDL_Rect *getRectTransform();

	virtual void setRectTransform(SDL_Rect *rectTransform);

	bool isActive() const;

	void setActive(bool active);

	void setScene(Scene *scene);

	/**
	 * Function for paint the gameObject
	 */
	virtual void paint();

	/**
	 * Destroy the object and remove it from the scene
	 */
	virtual void erase();

	/**
	 * Function called on scene loading,
	 * considered as the constructor of the
	 * NO drawing here
	 * Called only once on each scene loading
	 * @see GameObject.h
	 */
	virtual void awake();

	/**
	 * Function called just after scene loading
	 * Only called once after scene loaded
	 * NO drawing here
	 */
	virtual void start();

	/**
	 * Function called first on each game loop
	 * You can draw here
	 */
	virtual void update();

	/**
	 * Function called after update and before end of the game loop
	 * You can add your calculation, if depend on the current gameloop update
	 * You can draw here, don't draw in update and late update.
	 */
	virtual void lateUpdate();
};

#endif
