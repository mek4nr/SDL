#ifndef MUSHROOMWAR_GAMELOOP_H
#define MUSHROOMWAR_GAMELOOP_H

#include "../../Include.h"
#include "../../Utils/Singleton.h"
#include "GameObject.h"
#include "Scene.h"

class Scene;

class GameLoop : public Singleton<GameLoop> 
{
private:
	Scene* currentScene = nullptr; /// The current scene loaded
	Scene* newScene = nullptr; /// The new scene to load (during his loading)
	int width = 0; /// Width of the window
	int height = 0; /// Height of the window
	SDL_Window* window = nullptr; /// The current window
	SDL_Renderer* renderer = nullptr; /// The window renderer
	bool quit = false; /// Set to true for quit game
	SDL_Event event;
	std::vector<SDL_Event*> events = std::vector<SDL_Event *>(); /// List of all event catch during loop
	const Uint8 *state = nullptr; /// Keyboard state, input catch during loop

	static bool showLog; /// if true Show log message
	static bool showInfo; /// if true Show info message

	/**
	 * Call the start of the scene
	 */
	void startScene();

	/**
	 * Close a scene
	 */
	void closeScene();

	/**
	 * Call the scene loop
	 */
	void loopScene();

	/**
	 * Catch
	 * @param event
	 */
	void cathingEvent(SDL_Event* event);

	/**
	 * Catch all statement of quitting game
	 * @return
	 */
	bool exitEscape();

	/**
	 * Close the game, calling all quit from library
	 */
	void close();

public:
	int getWidth() const;
	int getHeight() const;
	std::vector<SDL_Event*> getEvents() { return events; };
	Scene* getScene() { return currentScene; }
	SDL_Renderer* getRenderer() { return renderer; }

	/**
	 * Windowed mode
	 * @param title  : title of the screen
	 * @param width : width of the screen
	 * @param height : height of the screen
	 */
	void init(std::string title, int width, int height, Scene* startingScene);

	/**
	 * Load a new scene, call init & load of the scene
	 * @param scene The scene to load
	 */
	void loadScene(Scene* scene);

	/**
	 * Called for quit the game
	 */
	void exit();

	/**
	 * Write a log message
	 */
	static void Log(std::string);

	/**
	 * Write an Info message
	 */
	static void Info(std::string);

	/**
	 * Write an error message
	 */
	static void Error(std::string);
};
#endif
