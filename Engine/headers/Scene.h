#ifndef MUSHROOMWAR_SCENE_H
#define MUSHROOMWAR_SCENE_H

#include "../../Include.h"
#include "GameObject.h"
#include "../Listener/MouseEvent.h"

class GameObject;
class IPointer;
class IPointerDown;
class IPointerUp;
class IPointerClick;
class IPointerDrag;

class Scene 
{
	friend class GameLoop;
private:
	std::vector<GameObject *> gameObjects; /// List of all gameobjects of the scene
	std::vector<IPointer *> iPointer; /// List of all gameobjects who catch pointer event
	std::vector<IPointerDown *> iPointerDown; /// List of all gameobjects who catch pointerdown event
	std::vector<IPointerUp *> iPointerUp; /// List of all gameobjects who catch pointerup event
	std::vector<IPointerClick *> iPointerClick; /// List of all gameobjects who catch click event
	std::vector<IPointerDrag *> iPointerDrag; /// List of all gameobjects who catch drag event
	bool isLoaded = false; /// If the scene finish called load function

	/**
	 * Load the scene and gameobject by calling awake of all gameobject
	 */
	void load();

	/**
	 * Start the scene, call start of all gameobject
	 */
	void start();

	/**
	 * After gameloop catching event, scene will call function for all gameobject who need it
	 */
	void catchingEvent();

	/**
	 * Scene loop, call in order : update lateupdate paint for all active gameobject
	 */
	void loop();

	/**
	 * Close the scene and call erase for all gameobjects
	 */
	void close();
protected:
	SDL_Renderer *renderer = nullptr; /// The renderer, get from gameloop

	/**

	*/
	virtual void startThread(); 

	/**
	* Init the scene, you need add gameobject needed on start here
	*/
	virtual void init();

	virtual void beforeClose();

public:
	std::vector<GameObject *> getGameObjects();
	bool IsLoaded() { return isLoaded; }

	/**
	 * Add a new game object to the scene. If this object catch event
	 * he will be add in the vector automaticaly
	 */
	void addGameObject(GameObject *, bool addToLoop=true);

	/**
	 * Remove a game object from the scene. And remove it from all vector event it is
	 */
	void removeGameObject(GameObject *);



	
};

#endif
