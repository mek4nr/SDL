#include "Tween.h"


int updateOrigin(void* data) 
{
    Tween* tween = (Tween*)data;

    while(!tween->finish)
    {
        SDL_Delay(30);
        tween->currentTime = SDL_GetTicks();
        tween->countedTime += tween->currentTime - tween->lastTime;

        if(!MathSDL::equals(tween->target, tween->lastTarget))
        {
            tween->Reset();
        }

        if(tween->countedTime >= tween->timeToFinish)
        {
            *(tween->origin) = *tween->target;
        }
        else
        {
            float percent = tween->countedTime / (float) tween->timeToFinish;

            tween->origin->x += (tween->target->x - tween->origin->x) * percent;
            tween->origin->y += (tween->target->y - tween->origin->y) * percent;
            tween->origin->w += (tween->target->w - tween->origin->w) * percent;
            tween->origin->h += (tween->target->h - tween->origin->h) * percent;

        }

        tween->lastTime = tween->currentTime;
    }
	return 1;
}



Tween::Tween(SDL_Rect *origin, SDL_Rect *target, unsigned long time) 
{
    timeToFinish = time;
    this->target = target;
    Reset();
    this->origin = origin;
    tween = SDL_CreateThread(updateOrigin, "Tween", (void*)this);
}

void Tween::Reset() {
    currentTime = lastTime = SDL_GetTicks();
    countedTime = 0;
    lastTarget = new SDL_Rect({
            target->x,
            target->y,
            target->w,
            target->h
      });
}

Tween::~Tween() {
    finish = true;
    SDL_DetachThread(tween);
}
