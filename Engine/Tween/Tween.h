#ifndef MUSHROOMWAR_TWEEN_H
#define MUSHROOMWAR_TWEEN_H

#include "../../Include.h"
#include "../headers/GameLoop.h"
#include "../../Utils/MathSDL.h"

class Tween
{
public:
	unsigned long currentTime = 0;
	unsigned long lastTime = 0;
	unsigned long countedTime = 0;
	unsigned long timeToFinish = 0;
	bool finish = false;
	SDL_Rect* target;
	SDL_Rect* lastTarget;
	SDL_Rect* origin;
	SDL_Thread* tween;

	Tween(SDL_Rect* origin, SDL_Rect* target, unsigned long time);
	void Reset();
	~Tween();
};

#endif //MUSHROOMWAR_TWEEN_H
