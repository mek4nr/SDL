#include "headers/Scene.h"

void Scene::addGameObject(GameObject *gameObject, bool addToLoop)
{
	if(addToLoop)
		gameObjects.push_back(gameObject);

    if (auto *ip = dynamic_cast<IPointer *>(gameObject)) 
	{
        GameLoop::Info("Ipointer catch");
        iPointer.push_back(ip);
    }
    if (auto *ip = dynamic_cast<IPointerUp *>(gameObject))
	{
        GameLoop::Info("IpointerUp catch");
        iPointerUp.push_back(ip);
    }
    if (auto *ip = dynamic_cast<IPointerDown *>(gameObject))
	{
        GameLoop::Info("IpointerDown catch");
        iPointerDown.push_back(ip);
    }
    if (auto *ip = dynamic_cast<IPointerClick *>(gameObject))
	{
        GameLoop::Info("IPointerClick catch");
        iPointerClick.push_back(ip);
    }
    if (auto *ip = dynamic_cast<IPointerDrag *>(gameObject)) 
	{
        GameLoop::Info("IPointerDrag catch");
        iPointerDrag.push_back(ip);
    }

    if(this->isLoaded)
    {
        gameObject->awake();
        gameObject->start();
    }
}

void Scene::removeGameObject(GameObject *gameObject)
{
	int cmp = 0;
	for (auto &go : gameObjects) // access by reference to avoid copying
	{
		if (go == gameObject) 
		{
			gameObjects.erase(gameObjects.begin() + cmp);
            break;
		}
		cmp++;
	}

    if (auto *ip = dynamic_cast<IPointer *>(gameObject))
	{
        cmp = 0;
        for (auto &go : iPointer) // access by reference to avoid copying
        {
            if (go == ip)
			{
                iPointer.erase(iPointer.begin() + cmp);
                break;
            }
            cmp++;
        }
    }
    if (auto *ip = dynamic_cast<IPointerUp *>(gameObject)) 
	{
        cmp = 0;
        for (auto &go : iPointerUp) // access by reference to avoid copying
        {
            if (go == ip) 
			{
                iPointerUp.erase(iPointerUp.begin() + cmp);
                break;
            }
            cmp++;
        }
    }
    if (auto *ip = dynamic_cast<IPointerDown *>(gameObject)) 
	{
        cmp = 0;
        for (auto &go : iPointerDown) // access by reference to avoid copying
        {
            if (go == ip) 
			{
                iPointerDown.erase(iPointerDown.begin() + cmp);
                break;
            }
            cmp++;
        }
    }
    if (auto *ip = dynamic_cast<IPointerClick *>(gameObject))
	{
        cmp = 0;
        for (auto &go : iPointerClick) // access by reference to avoid copying
        {
            if (go == ip)
			{
                iPointerClick.erase(iPointerClick.begin() + cmp);
                break;
            }
            cmp++;
        }
    }
    if (auto *ip = dynamic_cast<IPointerDrag *>(gameObject))
	{
        cmp = 0;
        for (auto &go : iPointerDrag) // access by reference to avoid copying
        {
            if (go == ip) {
                gameObjects.erase(gameObjects.begin() + cmp);
                break;
            }
            cmp++;
        }
    }
}

void Scene::startThread()
{
}

void Scene::init() 
{
    GameLoop::Info("Init Scene");
    isLoaded = false;
    this->renderer = GameLoop::Instance().getRenderer();
}

void Scene::load()
{
	GameLoop::Info("Load Scene");
	for (auto &go : gameObjects) 
	{
		if (go == nullptr) 
		{
			go = new GameObject();
		}
		if (go->isActive())
			go->awake();
	}
	isLoaded = true;
}

void Scene::start()
{
	GameLoop::Info("Start Scene");
	for (auto &go : gameObjects)
	{
		if (go->isActive())
			go->start();
	}
	startThread();
}

void Scene::catchingEvent()
{
	GameLoop::Info("Catching event");

	SDL_Point cursor;
	SDL_GetMouseState(&cursor.x, &cursor.y);

    for (auto &event : GameLoop::Instance().getEvents()) 
	{
        switch (event->type)
		{
            case SDL_MOUSEBUTTONDOWN:
                for (IPointerDown *ip : iPointerDown)
				{   // WHY dynanic cast marche et pas (GameObject*)ip ;
                    GameObject *g = dynamic_cast<GameObject *>(ip);
                    bool in = SDL_PointInRect(&cursor, g->getRectTransform());
                    if (in) 
					{
                        ip->onPointerDown({*event, cursor});
                    }
                }

                for (auto *ip : iPointerClick) 
				{
                    GameObject *g = dynamic_cast<GameObject *>(ip);
                    bool in = SDL_PointInRect(&cursor, g->getRectTransform());
                    if (in) 
					{
                        ip->setClicked(true, {*event, cursor});
                    }
                }

                for (auto *ip : iPointerDrag) 
				{
                    GameObject *g = dynamic_cast<GameObject *>(ip);
                    bool in = SDL_PointInRect(&cursor, g->getRectTransform());
                    if (in) 
					{
                        ip->setClicked(true, {*event, cursor});
                    }
                }
                break;
            case SDL_MOUSEBUTTONUP:
                for (auto *ip : iPointerUp)
				{
                    GameObject *g = dynamic_cast<GameObject *>(ip);
                    bool in = SDL_PointInRect(&cursor, g->getRectTransform());
                    if (in) 
					{
                        ip->onPointerUp({*event, cursor});
                    }
                }

                for (auto *ip : iPointerClick) 
				{
                    ip->setClicked(false, {*event, cursor});
                }

                for (auto *ip : iPointerDrag) 
				{
                    ip->setClicked(false, {*event, cursor});
                }
                break;
            case SDL_MOUSEMOTION:
                for (auto *ip : iPointer) 
				{
                    GameObject *g = dynamic_cast<GameObject *>(ip);
                    bool in = SDL_PointInRect(&cursor, g->getRectTransform());
                    ip->setIn(in, {*event, cursor});
                }

                for (auto *ip : iPointerDrag)
				{
                    ip->setDraging({*event, cursor});
                }
                break;
        }
    }
}

void Scene::loop() 
{
	GameLoop::Info("Update Scene");
	for (auto &go : gameObjects)
    {
		if (go->isActive())
			go->update();
	}
	GameLoop::Info("LateUpdate Scene");
	for (auto &go : gameObjects)
    {
		if (go->isActive())
			go->lateUpdate();
	}

	GameLoop::Info("Paint Scene");
	SDL_RenderClear(renderer);
	for (auto &go : gameObjects)
    {
		if (go->isActive())
			go->paint();
	}
	SDL_RenderPresent(renderer);
}

void Scene::beforeClose()
{

}

void Scene::close()
{
	GameLoop::Info("Close Scene");
	beforeClose();
	for (auto* go : gameObjects)
    {
        delete go;
	}
	SDL_Delay(100);
}

std::vector<GameObject *> Scene::getGameObjects()
{
	return gameObjects;
}