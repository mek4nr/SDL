#include "headers/GameObject.h"

void GameObject::setScene(Scene *scene)
{
	if (this->scene != nullptr) 
	{
		this->scene->removeGameObject(this);
	}
	this->scene = scene;
	this->scene->addGameObject(this);
}

void GameObject::awake()
{
	renderer = GameLoop::Instance().getRenderer();
}

void GameObject::start()
{
}

void GameObject::update()
{
}

void GameObject::lateUpdate()
{
}

void GameObject::paint()
{
}

void GameObject::erase()
{
	this->scene->removeGameObject(this);
	delete (this);
}

bool GameObject::isActive() const
{
    return active;
}

void GameObject::setActive(bool active)
{
	this->active = active;
}

SDL_Rect *GameObject::getRectTransform()
{
	return this->rectTransform;
}

void GameObject::setRectTransform(SDL_Rect *rectTransform)
{
	this->rectTransform = rectTransform;
}

GameObject::~GameObject() {
	delete rectTransform;
}
