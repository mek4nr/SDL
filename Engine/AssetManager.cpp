#include "headers/AssetManager.h"

SDL_Surface* AssetManager::LoadImage(std::string path)
{
	SDL_Surface* t = IMG_Load(("assets/img/" + path).c_str());
	GameLoop::Error(path);
	GameLoop::Error(IMG_GetError());
	return t;
}

TTF_Font* AssetManager::LoadFont(std::string path, int fontSize)
{
	TTF_Font* font = TTF_OpenFont(("assets/font/" + path).c_str(), fontSize);
	GameLoop::Error(path);
	GameLoop::Error(TTF_GetError());
	return font;
}

SDL_Texture* AssetManager::LoadImage(std::string path, SDL_Renderer* renderer)
{
	SDL_Surface* surface = LoadImage(path);
	SDL_Texture* t = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
	GameLoop::Error(IMG_GetError());
	return t;
}