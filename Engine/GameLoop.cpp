#include "headers/GameLoop.h"

bool GameLoop::showLog = true;
bool GameLoop::showInfo = false;

void GameLoop::init(std::string title, int width, int height, Scene *startingScene) {
    GameLoop::Info("[GAMELOOP] Init");
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
        this->width = width;
        this->height = height;
        TTF_Init();
        SDL_CreateWindowAndRenderer(
                width,
                height,
                SDL_WINDOW_SHOWN,
                &window,
                &renderer
        );

        SDL_SetWindowTitle(window, title.c_str());

        currentScene = startingScene;
        currentScene->init();
        currentScene->load();
        startScene();
    } else {
        GameLoop::Error("Error detected");
        GameLoop::Error(SDL_GetError());
    }
}

void GameLoop::startScene() {
    GameLoop::Info("[GAMELOOP] Start Scene");
    currentScene->start();
    loopScene();
}

void GameLoop::cathingEvent(SDL_Event *event) {
    this->events.clear();

    switch (event->type) {
        case SDL_MOUSEBUTTONDOWN:
            events.push_back(event);
            break;
        case SDL_MOUSEBUTTONUP:
            events.push_back(event);
            break;
        case SDL_MOUSEMOTION:
            events.push_back(event);
            break;
    }

    currentScene->catchingEvent();
}

bool GameLoop::exitEscape() {
    //GameLoop::Info("[GAMELOOP] Exit Escape");
    bool quit = false;
    for (auto e : events) {
        if (e->type == SDL_QUIT) {
            quit = true;
            break;
        }
    }

    return this->quit || quit || (state != nullptr && state[SDL_SCANCODE_ESCAPE]);
}

void GameLoop::loopScene() {
    while (!exitEscape()) {
        state = SDL_GetKeyboardState(nullptr);
        while (SDL_PollEvent(&event)) {
            cathingEvent(&event);
        }

        if (newScene != nullptr && newScene->IsLoaded()) {
            closeScene();
            break;
        } else {
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            currentScene->loop();

            SDL_Delay(1000 / 144);
        }
    };

    if (newScene != nullptr && newScene->IsLoaded()) {
        currentScene = newScene;
        newScene = nullptr;
        startScene();
    }
    if (quit)
        close();
}

void GameLoop::closeScene() {
    GameLoop::Info("[GAMELOOP] Close Scene");
    currentScene->close();
}

void GameLoop::loadScene(Scene *scene) {
    GameLoop::Info("[GAMELOOP] Load Scene");
    newScene = scene;
    newScene->init();
    newScene->load();
}

void GameLoop::exit() {
    this->quit = true;
}

void GameLoop::close() {
    GameLoop::Info("[GAMELOOP] Close");
    IMG_Quit();
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void GameLoop::Log(std::string os) {
    if (showLog)
        std::cout << "Debug : " << os << std::endl;
}

void GameLoop::Error(std::string os) {
    std::cerr << os << std::endl;
}

void GameLoop::Info(std::string os) {
    if (showInfo)
        std::cout << "Info : " << os << std::endl;
}

int GameLoop::getWidth() const {
    return width;
}

int GameLoop::getHeight() const {
    return height;
}