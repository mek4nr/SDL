#include "headers/AnimatedSprite.h"

void AnimatedSprite::paint()
{
	images[cmp]->paint();
}

void AnimatedSprite::update()
{
	currentTime = SDL_GetTicks();
	countTime += currentTime - lastTime;

	if (countTime > timePerFrame)
	{
		countTime -= timePerFrame;
		cmp++;
	}
	lastTime = currentTime;

	cmp = cmp%images.size();
}

void AnimatedSprite::addImage(std::string path)
{
	auto* img = new Image();
	img->setPath(path);
	img->setRectTransform(this->getRectTransform());
	img->awake();
	images.push_back(img);
}

void AnimatedSprite::setTimePerFrame(unsigned long t)
{
	timePerFrame = t;
}

void AnimatedSprite::awake()
{
	lastTime = SDL_GetTicks();
}