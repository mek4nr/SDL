#ifndef MUSHROOMWAR_TEXT_H
#define MUSHROOMWAR_TEXT_H

#include "../../../Include.h"
#include "../../headers/AssetManager.h"

class Text
{
private:
	std::string text = "text";
	std::string fontName = "";
	int fontSize = 0;
	SDL_Color fontColor;
	SDL_Texture* textureText = nullptr;
	TTF_Font* font = nullptr;
public:
	~Text();
	Text(std::string fontName, int fontSize, SDL_Color color);

	void setText(const std::string &text);

	void OpenFont();
	void TextureText(SDL_Renderer* renderer);
	void WriteText(SDL_Renderer* renderer, SDL_Rect* rect);
	void CloseFont();
};

#endif //MUSHROOMWAR_TEXT_H
