#ifndef MUSHROOMWAR_BUTTON_H
#define MUSHROOMWAR_BUTTON_H

#include "../../../Include.h"
#include "../../headers/GameObject.h"
#include "../../headers/GameLoop.h"
#include "../../../Utils/MathSDL.h"
#include "../../Listener/MouseEvent.h"
#include "Image.h"
#include "Text.h"

class Button : public GameObject, public IPointerDown, public IPointer
{
private:
	Text* textHover = nullptr;
	Text* textNormal = nullptr;
	Text* text = nullptr;
	Image* background = nullptr;
	Image* hover = nullptr;
	Image* normal = nullptr;
	Image *selected = nullptr;
	std::string textContent = "";
	bool isSelected = false;
protected:
	SDL_Rect* rectText = nullptr;
public:
	SDL_Rect *getRectText() const;
	void setRectText(SDL_Rect *rectText);
	void setImageHover(std::string path, SDL_Color* color = nullptr);
	void setImageNormal(std::string path, SDL_Color* color = nullptr);
	void setImageSelected(std::string path, SDL_Color* color = nullptr);
	void setRectTransform(SDL_Rect *rectTransform) override;
	void setTextHover(Text* text);
	void setTextNormal(Text* text);
	void setText(std::string text);
	virtual void setSelected(bool selected);

	void awake() override;
	void paint() override;
	void erase() override;
	void OnPointerEnter(MouseEvent eventData) override;
	void OnPointerExit(MouseEvent eventData) override;
};

#endif //MUSHROOMWAR_BUTTON_H
