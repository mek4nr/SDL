#ifndef MUSHROOMWAR_ANIMATEDSPRITE_H
#define MUSHROOMWAR_ANIMATEDSPRITE_H

#include "../../../Include.h"
#include "../../headers/GameObject.h"
#include "Image.h"

class AnimatedSprite : public GameObject
{
private:
	std::vector<Image*> images;
	unsigned long timePerFrame = 200;
	unsigned long lastTime = 0;
	unsigned long countTime = 0;
	unsigned long currentTime = 0;
	unsigned int cmp = 0;
public:
	void setTimePerFrame(unsigned long t);

	void addImage(std::string path);
	void awake() override;
	void paint() override;
	void update() override;
};

#endif //MUSHROOMWAR_ANIMATEDSPRITE_H
