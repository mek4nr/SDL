#ifndef MUSHROOMWAR_IMAGE_H
#define MUSHROOMWAR_IMAGE_H

#include "../../../Include.h"
#include "../../headers/GameObject.h"
#include "../../headers/AssetManager.h"

class Image : public GameObject
{
protected:
	SDL_Texture* image = nullptr;
	SDL_Color* backgroundColor = nullptr;
	std::string path = "";
public:
	~Image() override ;
	SDL_Color *getBackgroundColor() const;
	void setBackgroundColor(SDL_Color *backgroundColor);
	SDL_Texture *getImageTexture() const;
	std::string getPath();
	void setPath(std::string path);

	void awake() override;
	void paint() override;
};

#endif //MUSHROOMWAR_IMAGE_H
