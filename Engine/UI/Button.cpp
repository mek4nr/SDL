#include "headers/Button.h"

void Button::paint() {
    normal->paint();

    if (background != nullptr)
        background->paint();

    if (text != nullptr) {
        SDL_Rect r = MathSDL::margingAllRect(rectTransform, rectText);
        text->WriteText(renderer, &r);
    }
}

void Button::awake() {
    GameObject::awake();
    background = normal;
    text = textNormal;
}

void Button::erase() {
    if (text != nullptr)
        text->CloseFont();

    if (textHover != nullptr)
        textHover->CloseFont();

    if (textNormal != nullptr)
        textNormal->CloseFont();
    GameObject::erase();
}

SDL_Rect *Button::getRectText() const {
    return rectText;
}

void Button::setSelected(bool selected) {
    if (selected)
        this->background = this->selected;
    else
        this->background = this->normal;

    this->isSelected = selected;
}

void Button::setRectText(SDL_Rect *rectText) {
    this->rectText = rectText;
}

void Button::setImageHover(std::string path, SDL_Color *color) {
    this->hover = new Image();
    this->hover->setPath(path);
    this->hover->setBackgroundColor(color);
    this->hover->setRectTransform(this->getRectTransform());
}

void Button::setImageNormal(std::string path, SDL_Color *color) {
    this->normal = new Image();
    this->normal->setPath(path);
    this->normal->setBackgroundColor(color);
    this->normal->setRectTransform(this->getRectTransform());
    this->normal->awake();
}

void Button::setImageSelected(std::string path, SDL_Color *color) {
    this->selected = new Image();
    this->selected->setPath(path);
    this->selected->setBackgroundColor(color);
    this->selected->setRectTransform(this->getRectTransform());
    this->selected->awake();
}

void Button::setRectTransform(SDL_Rect *rectTransform) {
    GameObject::setRectTransform(rectTransform);

    if (hover != nullptr)
        hover->setRectTransform(rectTransform);
    if (normal != nullptr)
        normal->setRectTransform(rectTransform);
}

void Button::setTextHover(Text *text) {
    this->textHover = text;
    this->textHover->setText(textContent);
}

void Button::setTextNormal(Text *text) {
    this->textNormal = text;
    this->textNormal->setText(textContent);
}

void Button::setText(std::string text) {
    textContent = text;
    if (textHover != nullptr)
        textHover->setText(text);
    if (textNormal != nullptr)
        textNormal->setText(text);
}

void Button::OnPointerEnter(MouseEvent eventData) {
    if (!this->isSelected) {
        if (hover != nullptr)
            background = hover;

        if (textHover != nullptr)
            text = textHover;
    }
}

void Button::OnPointerExit(MouseEvent eventData) {
    if (!this->isSelected) {
        if (normal != nullptr)
            background = normal;

        if (textNormal != nullptr)
            text = textNormal;
    }
}