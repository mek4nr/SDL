#include "headers/Text.h"


Text::Text(std::string fontName, int fontSize, SDL_Color color)
{
	this->fontName = fontName;
	this->fontSize = fontSize;
	this->fontColor = color;
	this->OpenFont();
}

void Text::OpenFont() 
{
	font = AssetManager::LoadFont(fontName, fontSize);
}

void Text::CloseFont() 
{
	if(textureText != nullptr)
		SDL_DestroyTexture(textureText);
	TTF_CloseFont(font);
}

void Text::TextureText(SDL_Renderer* renderer) 
{
	if (font == nullptr)
		OpenFont();

	SDL_Surface* surfaceText = TTF_RenderText_Solid(font, text.c_str(), fontColor);

	if(textureText != nullptr)
		SDL_DestroyTexture(textureText);

	textureText = SDL_CreateTextureFromSurface(renderer, surfaceText);
	SDL_FreeSurface(surfaceText);
	
}

void Text::WriteText(SDL_Renderer* renderer, SDL_Rect* rect) 
{
	TextureText(renderer);
	SDL_RenderCopy(renderer, textureText, NULL, rect);
}

void Text::setText(const std::string &text)
{
	Text::text = text;
}

Text::~Text() {
	CloseFont();
}
