#include "headers/Image.h"

SDL_Texture *Image::getImageTexture() const {
    return this->image;
}

SDL_Color *Image::getBackgroundColor() const {
    return this->backgroundColor;
}

void Image::setBackgroundColor(SDL_Color *backgroundColor) {
    this->backgroundColor = backgroundColor;
}

std::string Image::getPath() {
    return this->path;
}

void Image::setPath(std::string path) {
    this->path = path;
}

void Image::awake() {
    GameObject::awake();
    SDL_DestroyTexture(image);
    this->image = AssetManager::LoadImage(this->path, this->renderer);
}

void Image::paint() {
    GameObject::paint();
    SDL_RenderCopy(this->renderer, this->image, NULL, this->rectTransform);
}

Image::~Image() {
    SDL_DestroyTexture(this->image);
    delete backgroundColor;
    this->path.clear();
}
