#include "MouseEvent.h"

bool IPointer::isIn() const
{
	return in;
}

void IPointer::setIn(bool isIn, MouseEvent eventdata)
{
	if (isIn && !this->in)
	{
		OnPointerEnter(eventdata);
		in = !in;
	}
	else if (!isIn && this->in)
	{
		OnPointerExit(eventdata);
		in = !in;
	}
}

bool IPointerClick::isClicked()
{
	return clicked;
}

void IPointerClick::setClicked(bool clicked, MouseEvent eventData)
{
	if (clicked)
	{
		this->clicked = true;
		this->onClickDown(eventData);
	}
	else if (!clicked && this->clicked)
	{
		this->onClickUp(eventData);
	}
}

void IPointerDrag::setDraging(MouseEvent eventData)
{
	if (this->clicked)
	{
		this->onDrag(eventData);
	}
}

void IPointerDrag::setClicked(bool clicked, MouseEvent eventData)
{
	if (clicked)
	{
		this->clicked = true;
		this->onDragBegin(eventData);
	}
	else if (!clicked && this->clicked)
	{
		this->clicked = false;
		this->onDragEnd(eventData);
	}
}