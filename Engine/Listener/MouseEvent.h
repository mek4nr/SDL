#ifndef MUSHROOMWAR_IPOINTERDOWN_H
#define MUSHROOMWAR_IPOINTERDOWN_H

#include "../../Include.h"
#include "../headers/GameLoop.h"

struct MouseEvent {
    const SDL_Event event;
    const SDL_Point cursor;
};

class IPointerDown {
public:
    virtual void onPointerDown(MouseEvent eventData) {
        std::cerr << ("You need to define pointer down");
    };
};

class IPointerUp {
public:
    virtual void onPointerUp(MouseEvent eventData) {
        std::cerr << ("You need to define pointer up");
    };
};

class IPointerClick {
private:
    bool clicked = false;
public:
    bool isClicked();

    void setClicked(bool clicked, MouseEvent eventData);

    virtual void onClickDown(MouseEvent eventData) {
        std::cerr << ("You need to define pointer click on click down");
    };

    virtual void onClickUp(MouseEvent eventData) {
        std::cerr << ("You need to define pointer click on click up");
    };
};

class IPointerDrag {
private:
    bool clicked = false;
public:
    void setClicked(bool clicked, MouseEvent eventData);

    void setDraging(MouseEvent eventData);

    virtual void onDragBegin(MouseEvent eventData) {
        std::cerr << ("You need to define pointer drag begin");
    };

    virtual void onDragEnd(MouseEvent eventData) {
        std::cerr << ("You need to define pointer drag end");
    };

    virtual void onDrag(MouseEvent eventData) {
        std::cerr << ("You need to define pointer drag");
    };
};

class IPointer {
private:
    bool in;
public:
    bool isIn() const;

    void setIn(bool isIn, MouseEvent eventData);

    virtual void OnPointerEnter(MouseEvent eventData) {
        std::cerr << ("You need to define pointer enter");
    };

    virtual void OnPointerExit(MouseEvent eventData) {
        std::cerr << ("You need to define pointer exit");
    };
};

#endif