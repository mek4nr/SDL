#include "MathSDL.h"
#include "../Engine/headers/GameLoop.h"

SDL_Rect MathSDL::additionRect(SDL_Rect *r1, SDL_Rect *r2) {
	SDL_Rect r = { 0,0,0,0 };
	r.x = r1->x + r2->x;
	r.y = r1->y + r2->y;
	r.w = r1->w + r2->w;
	r.h = r1->h + r2->h;

	return r;
}

SDL_Rect MathSDL::substractionRect(SDL_Rect *r1, SDL_Rect *r2) {
	SDL_Rect r = { 0,0,0,0 };
	r.x = r1->x - r2->x;
	r.y = r1->y - r2->y;
	r.w = r1->w - r2->w;
	r.h = r1->h - r2->h;

	return r;
}

SDL_Rect MathSDL::margingAllRect(SDL_Rect *r1, SDL_Rect *r2) {
	SDL_Rect r = { 0,0,0,0 };
	r.x = r1->x + r2->x;
	r.y = r1->y + r2->y;
	r.w = r1->w - r2->w;
	r.h = r1->h - r2->h;

	return r;
}

SDL_Rect MathSDL::relativePosition(SDL_Rect *r1, SDL_Rect *r2) {
	SDL_Rect r = { 0,0,0,0 };
	r.x = r1->x + r2->x;
	r.y = r1->y + r2->y;
	r.w = r1->w;
	r.h = r1->h;
	return r;
}

void MathSDL::centerWidth(int width, SDL_Rect *r) {
	r->x = width / 2 + r->w;
}

void MathSDL::centerHeight(int height, SDL_Rect *r) {
	r->y = height / 2 + r->h;
}

void MathSDL::percentRect(SDL_Rect *r1, int width, int height, EAnchorPosition position)
{
	r1->w = float(r1->w) / 100 * width;
	r1->h = float(r1->h) / 100 * height;
	switch (position)
	{
	case Center:
		r1->x = float(r1->x) / 100 * width - r1->w / 2;
		r1->y = float(r1->y) / 100 * height - r1->h / 2;
		break;
	case TopRight:
		r1->x = float(r1->x) / 100 * width - r1->w;
		r1->y = float(r1->y) / 100 * height;
		break;
	default:
		r1->x = float(r1->x) / 100 * width;
		r1->y = float(r1->y) / 100 * height;
	}
}

void MathSDL::scaleRectFromFHD(SDL_Rect *r1) {
	int width = GameLoop::Instance().getWidth();
	int height = GameLoop::Instance().getHeight();

	r1->w = float(r1->w) / 1920 * width;
	r1->h = float(r1->h) / 1080 * height;
}

int MathSDL::scaleFromFHD(int length) {
	int width = GameLoop::Instance().getWidth();

	return float(length) / 1920 * width;
}

void MathSDL::getEquationBetweenTwoPoints(SDL_Point *p1, SDL_Point *p2, float *x, float *y) {
	float dx = p2->x - p1->x;
	float dy = p2->y - p1->y;

	float slope = dy / dx;
	float intercept = p1->y - slope * p1->x;

	(*x) = slope;
	(*y) = intercept;
}

float MathSDL::clamp(float value, float min, float max)
{
	if (value > max) value = max;
	if (value < min) value = min;
	return value;
}

float MathSDL::clamp(int value, int min, int max) {
	if (value > max) value = max;
	if (value < min) value = min;
	return value;
}

SDL_Point * MathSDL::factorPoint(Point p, int width, int height) {
	SDL_Point* point = new SDL_Point{(int)(p.x * width), (int)(p.y * height)};
	return point;
}


