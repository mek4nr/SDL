#ifndef MUSHROOMWAR_SINGLETON_H
#define MUSHROOMWAR_SINGLETON_H

#include "../Include.h"

template <class T> class Singleton {
public:
	static T& Instance()
	{
		if (instance == nullptr)
		{
			instance = new T();
		}
		return *instance;
	}

protected:
	inline Singleton() {}
	virtual ~Singleton() {}

private:
	static T* instance;
};

template<class T>
T* Singleton<T>::instance = nullptr;

#endif //MUSHROOMWAR_SINGLETON_H
