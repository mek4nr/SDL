#ifndef MUSHROOMWAR_MATHSDL_H
#define MUSHROOMWAR_MATHSDL_H

#include "../Include.h"

enum EAnchorPosition {
    Center,
    TopLeft,
    TopRight,
};

struct Point {
    float x, y;
};

struct Vector2 {
    int x, y;

    Vector2(int x, int y) {
        this->x = x;
        this->y = y;
    }

    Vector2() {
        this->x = 0;
        this->y = 0;
    }
};

class MathSDL {
public:

    static Vector2 *vector(SDL_Point *A, SDL_Point *B) {
        return new Vector2{B->x - A->x, B->y - A->y};
    }

    static SDL_Rect additionRect(SDL_Rect *r1, SDL_Rect *r2);

    static SDL_Rect substractionRect(SDL_Rect *r1, SDL_Rect *r2);

    static SDL_Rect margingAllRect(SDL_Rect *r1, SDL_Rect *r2);

    static SDL_Rect relativePosition(SDL_Rect *r1, SDL_Rect *r2);

    static void centerWidth(int width, SDL_Rect *r);

    static void centerHeight(int height, SDL_Rect *r);

    static void percentRect(SDL_Rect *r1, int width, int height, EAnchorPosition position);

    static void scaleRectFromFHD(SDL_Rect *r1);

    static int scaleFromFHD(int length);

    static void getEquationBetweenTwoPoints(SDL_Point *p1, SDL_Point *p2, float *x, float *y);

    static float clamp(float value, float min, float max);

    static float clamp(int value, int min, int max);

    static SDL_Point *factorPoint(Point p, int width, int height);

    static bool equals(SDL_Rect *rect, SDL_Rect *rect1) {
        return (rect->x == rect1->x) &&
               (rect->y == rect1->y) &&
               (rect->w == rect1->w) &&
               (rect->h == rect1->h);
    }

    static int dot(Vector2 v1, Vector2 v2) {
        return v1.x * v2.x + v1.y * v2.y;
    }

    static int cross(Vector2 *v1, Vector2 *v2) {
        return v1->x * v2->y - v1->y * v2->x;
    }

    static SDL_Rect *createRect(SDL_Point *left, SDL_Point *right) {
        return new SDL_Rect{
                left->x,
                left->y,
                right->x - left->x,
                right->y - left->y
        };
    }

    static long squareDistance(SDL_Point *p1, SDL_Point *p2) {
        return (p1->x - p2->x) * (p1->x - p2->x) + (p1->y - p2->y) * (p1->y - p2->y);
    }
};

#endif
