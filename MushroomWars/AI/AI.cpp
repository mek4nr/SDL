#include "AI.h"
#include "AIEasy.h"

int threadAI(void* data)
{
	AI* ai = (AI*)data;
	
	while (ai != nullptr && ai->active)
	{
		ai->update();
		SDL_Delay(30);
	}

	return 0;
}

AI::AI()
{
}

void AI::start()
{
	GameLoop::Log("Thread AI created");
	if (ai != nullptr)
	{
		SDL_DetachThread(ai);
	}
	ai = SDL_CreateThread(threadAI, "AI", (void*)this);
}

void AI::stop()
{
	active = false;
}

AI::~AI() {
	active = false;
	SDL_DetachThread(ai);
}