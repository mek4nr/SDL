#ifndef MUSHROOMWAR_AIEASY_H
#define MUSHROOMWAR_AIEASY_H

#include "../../Include.h"
#include "../../Utils/Singleton.h"
#include "AI.h"
#include "../Game/GameObjects/headers/Mushroom.h"

class AIEasy : public AI
{
private:
	unsigned long currentTime = 0;
	unsigned long countTime = 0;
	unsigned long lastTime = 0;
	unsigned long timeForAction = 4000;
	EMushroomColor color = Green;

	void randomAttack();
	Mushroom* maxPowerMushroom();
	Mushroom* lessPowerMushroom();
	bool upgradeOne();

public:
	AIEasy();
	void init() override;
	void update() override;
};

#endif