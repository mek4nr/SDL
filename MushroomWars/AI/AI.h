#ifndef MUSHROOMWAR_AI_H
#define MUSHROOMWAR_AI_H

#include "../../Include.h"
#include "../../Utils/Singleton.h"
#include "../../Engine/headers/GameLoop.h"

enum EDifficulty
{
	Easy
};

class AI : public Singleton<AI>
{
private:
	SDL_Thread* ai;
public:
	bool active = true;
	AI();
	virtual void init() = 0;
	virtual void start();
	virtual void update() = 0;
	virtual void stop();
	virtual ~AI();
};

#endif