#include "AIEasy.h"

void AIEasy::randomAttack()
{
	Mushroom* low = lessPowerMushroom();
	Mushroom* high = maxPowerMushroom();
	if (high != nullptr && low != high)
	{
		high->sendUnits(low, 50);
	}
}

Mushroom* AIEasy::maxPowerMushroom()
{
	int max = 0;
	Mushroom* selected = nullptr;

	for (auto* m : Mushroom::mushrooms)
	{
		if (m != nullptr && max < m->getNbUnits() && m->getColor() == this->color)
		{
			max = m->getNbUnits();
			selected = m;
		}
	}

	return selected;
}

Mushroom* AIEasy::lessPowerMushroom()
{
	int min = 9999;
	Mushroom* selected = nullptr;

	for (auto* m : Mushroom::mushrooms)
	{
		if (m != nullptr && min > m->getNbUnits() && m->getColor() != this->color)
		{
			min = m->getNbUnits();
			selected = m;
		}
	}

	return selected;
}

bool AIEasy::upgradeOne()
{
	bool upgrade = false;

	for (auto* m : Mushroom::mushrooms)
	{
		if (m != nullptr && m->getColor() == this->color && m->isLevelUpPossible())
		{
			m->levelUp();
			upgrade = true;
		}
	}

	return upgrade;
}

AIEasy::AIEasy()
{
}

void AIEasy::init()
{
	this->lastTime = SDL_GetTicks();
}

void AIEasy::update()
{
	this->currentTime = SDL_GetTicks();
	this->countTime += this->currentTime - this->lastTime;
	if (this->countTime > this->timeForAction)
	{
		if(!upgradeOne())
			this->randomAttack();
		this->countTime = 0;
	}
	this->lastTime = SDL_GetTicks();
	
}
