#ifndef MUSHROOMWAR_MUSHROOMPATH_H
#define MUSHROOMWAR_MUSHROOMPATH_H

#include "../../Include.h"
#include "../Game/GameObjects/headers/Mushroom.h"

struct AvoidingZone
{
	SDL_Point* first;
	SDL_Point* second;
};

class Zone
{
public:
	virtual bool isInZone(SDL_Point *C) = 0;
	virtual AvoidingZone getMoves(SDL_Point *C, SDL_Point *end) = 0;
};

class RectZoneWith2SideEntry : public Zone
{
private:
	SDL_Rect* zone;
	SDL_Point* A;
	Vector2* AB;
	AvoidingZone leftEntry; // first point is out
	AvoidingZone rightEntry; // first point is out
public:
	RectZoneWith2SideEntry(SDL_Rect *zone, SDL_Point* A, Vector2* AB, const AvoidingZone &leftEntry, const AvoidingZone &rightEntry) :
		zone(zone), A(A), AB(AB), leftEntry(leftEntry), rightEntry(rightEntry)
	{}

	bool isInZone(SDL_Point* C) override {
		return SDL_PointInRect(C, zone);
	}

	AvoidingZone getMoves(SDL_Point *start, SDL_Point *end) override {
		if (isInZone(start))
		{
			AvoidingZone a = (MathSDL::cross(AB, MathSDL::vector(A, end)) < 0) ? leftEntry : rightEntry;
			std::swap(a.first, a.second);
			return a;
		}
		else
		{
			AvoidingZone a = (MathSDL::cross(AB, MathSDL::vector(A, start)) < 0) ? leftEntry : rightEntry;
			return a;
		}
	}
};

class PathLine : public Zone
{
private:
	SDL_Point *A;
	Vector2 *AB;
	SDL_Point *belowAB;
	SDL_Point *aboveAB;
public:
	PathLine(SDL_Point *A, Vector2 *AB, SDL_Point* belowAB, SDL_Point* aboveAB)
		: A(A), AB(AB), belowAB(belowAB), aboveAB(aboveAB)
	{}

	bool isInZone(SDL_Point *C) override {
		return MathSDL::cross(AB, MathSDL::vector(A, C)) < 0;
	}

	AvoidingZone getMoves(SDL_Point *start, SDL_Point *end) override {
		return (isInZone(start)) ? AvoidingZone{ aboveAB, belowAB } : AvoidingZone{ belowAB, aboveAB };
	}
};

class MushroomPath {
public:
	std::vector<Zone*> zones;

	bool sort(AvoidingZone* first, AvoidingZone* second, SDL_Point* start)
	{
		long cheminOrder =
			MathSDL::squareDistance(start, first->first) +
			MathSDL::squareDistance(first->first, first->second) +
			MathSDL::squareDistance(first->second, second->first) +
			MathSDL::squareDistance(second->first, second->second);

		long cheminNoOrder =
			MathSDL::squareDistance(start, second->first) +
			MathSDL::squareDistance(second->first, second->second) +
			MathSDL::squareDistance(second->second, first->first) +
			MathSDL::squareDistance(first->first, first->second);

		return(cheminNoOrder < cheminOrder);
	}

	std::vector<SDL_Point*> getMoves(SDL_Point* A, SDL_Point* B)
	{
		std::vector<SDL_Point*> v;
		std::vector<AvoidingZone> avoidingZone;

		for (auto* zone : zones)
		{
			bool a = zone->isInZone(A);
			bool b = zone->isInZone(B);

			if (a != b)
			{
				avoidingZone.push_back(zone->getMoves(A, B));
			}
		}

		if (avoidingZone.size() == 2)
		{
			//Marche pour un cas de taille 2, pour generaliser il faut prendre la plus petite distance et supprimer du tableau et repeter
			//Marche dans notre cas, peut ne pas marcher avec des falaises
			if (sort(&avoidingZone[0], &avoidingZone[1], A))
			{
				std::reverse(avoidingZone.begin(), avoidingZone.end());
			}
		}

		for (auto i : avoidingZone)
		{
			v.push_back(i.first);
			v.push_back(i.second);
		}

		return v;
	}
};

#endif //MUSHROOMWAR_MUSHROOMPATH_H
