#ifndef MUSHROOMWAR_LEVEL1_H
#define MUSHROOMWAR_LEVEL1_H

#include "../../../Include.h"
#include "../../AI/MushroomPath.h"

class Level1AI : public Singleton<Level1AI>
{
private:
	MushroomPath mushroomPath;
public:
	std::vector<SDL_Point*> GetCheckpoints(SDL_Point* start, SDL_Point* end)
	{
		return mushroomPath.getMoves(start, end);
	}

	Level1AI() {
		SDL_Point* avoidPoint[8] = {
			//Left river
			MathSDL::factorPoint({ 0.25f, 0.49f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			MathSDL::factorPoint({ 0.33f, 0.42f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			//Right river
			MathSDL::factorPoint({ 0.67f, 0.41f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			MathSDL::factorPoint({ 0.75f, 0.48f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			//Left rift
			MathSDL::factorPoint({ 0.20f, 0.12f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			MathSDL::factorPoint({ 0.30f, 0.09f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			//Right rift
			MathSDL::factorPoint({ 0.65f, 0.08f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			MathSDL::factorPoint({ 0.77f, 0.11f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
		};

		SDL_Point* line[8] = {
			//Left river
			MathSDL::factorPoint({ 0.07f, 0.08f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			MathSDL::factorPoint({ 0.47f, 0.95f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			//Right river
			MathSDL::factorPoint({ 0.89f, 0.18f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			MathSDL::factorPoint({ 0.55f, 0.93f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			//Rift Rect
			MathSDL::factorPoint({ 0.34f, 0.03f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			MathSDL::factorPoint({ 0.64f, 0.35f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			//Rift scalaire
			MathSDL::factorPoint({ 0.50f, 0.03f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
			MathSDL::factorPoint({ 0.51f, 0.30f }, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
		};

		mushroomPath = MushroomPath();
		std::vector<Zone*> zones;

		zones.push_back(new PathLine(line[0], MathSDL::vector(line[0], line[1]), avoidPoint[0], avoidPoint[1]));
		zones.push_back(new PathLine(line[2], MathSDL::vector(line[2], line[3]), avoidPoint[2], avoidPoint[3]));
		zones.push_back(new RectZoneWith2SideEntry(
			MathSDL::createRect(line[4], line[5]),
			line[7],
			MathSDL::vector(line[7], line[6]),
			AvoidingZone{ avoidPoint[4], avoidPoint[5] },
			AvoidingZone{ avoidPoint[7], avoidPoint[6] }
		));
		mushroomPath.zones = zones;
	}
};

#endif