#ifndef MUSHROOM_WARS_GAMESCENE_H
#define MUSHROOM_WARS_GAMESCENE_H

#include "../../../Include.h"
#include "../../../Engine/headers/Scene.h"
#include "../../../Engine/UI/headers/Image.h"
#include "../../../Utils/MathSDL.h"
#include "../../AI/AIEasy.h"
#include "../../AI/AI.h"
#include "../GameObjects/headers/UnitDirection.h"
#include "../GameObjects/headers/Mushroom.h"
#include "../GameObjects/headers/Unit.h"
#include "../GameObjects/headers/ButtonMainMenu.h"
#include "../GameObjects/headers/UIUnitCounter.h"

class GameScene : public Scene {
public:
	typedef Scene super;


private:
	AIEasy* ai = new AIEasy();
	SDL_Point *startCursor;
	bool clicked = false;

	void createMushroom(Point p, EMushroomColor color, EMushroomType type, int level, int startUnit);

protected:
	UnitDirection *direction;
	void init() override;
	void startThread() override;
	void beforeClose() override;
};

#endif //MUSHROOM_WARS_GAMESCENE_H
