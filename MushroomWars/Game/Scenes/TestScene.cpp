#include "TestScene.h"
#include "../GameObjects/headers/GridMap.h"
#include "../GameObjects/headers/Mushroom.h"

void TestScene::init()
{
    super::init();
    GameLoop::Info("[GAMESCENE] init()");

    GridMap* gm = new GridMap("map.jpg");
    gm->setScene(this);

}