#include "GameScene.h"
#include "../GameObjects/headers/ButtonUnitPercent.h"

void GameScene::init()
{
	super::init();
	GameLoop::Info("[GAMESCENE] init()");

	this->direction = new UnitDirection();

	Image *background = new Image();
	background->setPath("map.jpg");
	background->setRectTransform(nullptr);
	background->setScene(this);
	
	//TODO : les maisons sont pas bien place, meme a 0.10% on est pas au bord de la map a revoir
	//Left river
	auto p = Point{ 0,0 };
	
	p = {0.07f, 0.35f};
	createMushroom(p, Green, Village, 1, 10);
	
	p = {0.18f, 0.39f};
	createMushroom(p, Green, Village, 1, 10);
	
	p = { 0.12f, 0.60f };
	createMushroom(p, Orange, Village, 1, 10);
	//
	//    p = {0.178, 0.841};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	//    p = {0.324, 0.762};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	//    // Top falaise
	//    p = {0.175, 0.115};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	//    p = {0.341, 0.157};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	p = { 0.48f, 0.21f };
	createMushroom(p, Orange, Village, 2, 10);
	//
	//    p = {0.646, 0.147};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	//    p = {0.831, 0.113};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	//    // Bottom cliff
	//    p = {0.383, 0.526};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	p = { 0.45f, 0.65f };
	createMushroom(p, Orange, Village, 2, 10);
	//
	//    p = {0.63, 0.505};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	//
	//    // Right river
	//    p = {0.94, 0.264};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	//    p = {0.836, 0.421};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	p = { 0.95f, 0.60f };
	createMushroom(p, Grey, Forge, 2, 10);
	//
	//    p = {0.0704, 0.771};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);
	//
	//    p = {0.857, 0.894};
	//    createMushroom(p, Mushroom::MUSHROOM_GREY, 2, 10);

	auto menu = new ButtonMainMenu();
	menu->setScene(this);

	auto unitCounter = new UIUnitCounter();
	auto* rUnitCounter = new SDL_Rect{ 30,0,70,5 };

	MathSDL::percentRect(rUnitCounter, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight(), TopLeft);
	unitCounter->setRectTransform(rUnitCounter);
	unitCounter->setScene(this);

	auto percent100 = new ButtonUnitPercent(ButtonUnitPercent::PERCENT100);
	auto percent75 = new ButtonUnitPercent(ButtonUnitPercent::PERCENT75);
	auto percent50 = new ButtonUnitPercent(ButtonUnitPercent::PERCENT50);
	auto percent25 = new ButtonUnitPercent(ButtonUnitPercent::PERCENT25);

	percent100->setScene(this);
	percent75->setScene(this);
	percent50->setScene(this);
	percent25->setScene(this);

	percent100->setSelected(true);

	this->direction->setActive(false);
	this->direction->setScene(this);
	this->direction->awake();
	this->direction->start();

	ai->init();
}

void GameScene::createMushroom(Point p, EMushroomColor color, EMushroomType type, int level, int startUnit) {
	SDL_Point* point = MathSDL::factorPoint(p, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight());
	(new Mushroom(color, point->x, point->y, level, startUnit, type, this->direction))->setScene(this);
}

void GameScene::startThread()
{
	ai->start();
}

void GameScene::beforeClose()
{
	ButtonUnitPercent::selectedButton = nullptr;
}
