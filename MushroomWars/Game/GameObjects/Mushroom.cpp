#include "headers/Mushroom.h"
#include "headers/ButtonUnitPercent.h"

std::vector<Mushroom *> Mushroom::mushrooms;

Mushroom::Mushroom(EMushroomColor color, int x, int y, int level, int nbUnits, EMushroomType type, UnitDirection *direction) :
	color(color), x(x), y(y), level(level), nbUnits(nbUnits), type(type), direction(direction) {
	
	this->canLevelUp = false;
	Mushroom::mushrooms.push_back(this);
	this->generateImage();
	this->sem = SDL_CreateSemaphore(1);

    this->canLevelUp = false;
    Mushroom::mushrooms.push_back(this);
    this->generateImage();

    this->bubble = new Image();
    this->bubble->setPath("bubble.png");

    this->levelUpButton = new ButtonUpgrade(this);
    this->forgeButton = new ButtonForge(this);
    this->villageButton = new ButtonVillage(this);

    SDL_Rect *rect1 = new SDL_Rect({-25, -25, 40, 40});
    SDL_Rect *bubblePos = new SDL_Rect();
    *bubblePos = MathSDL::relativePosition(rect1, this->rectTransform);

    this->bubble->setRectTransform(bubblePos);
    this->bubble->awake();

    SDL_Rect *levelUp = new SDL_Rect();
    *levelUp = (MathSDL::relativePosition(
            new SDL_Rect({-20, 60, 40, 40}), this->rectTransform
    ));

    this->levelUpButton->setRectTransform(levelUp);

    SDL_Rect *forge = new SDL_Rect();
    *forge = (MathSDL::relativePosition(
            new SDL_Rect({40, 60, 40, 40}), this->rectTransform
    ));

    this->forgeButton->setRectTransform(forge);

    SDL_Rect *village = new SDL_Rect();
    *village = (MathSDL::relativePosition(
            new SDL_Rect({-20, 60, 40, 40}), this->rectTransform
    ));

    this->villageButton->setRectTransform(village);

    this->unitText = new Text("obelixpro/ObelixPro-cyr.ttf", 12, {0, 0, 0, 255});
    this->lastTime = SDL_GetTicks();

    this->startCursor = new SDL_Point();
}

void Mushroom::awake() {
    this->scene->addGameObject(this->levelUpButton, false);
    this->scene->addGameObject(this->forgeButton, false);
    this->scene->addGameObject(this->villageButton, false);
}

int Mushroom::nbForge(EMushroomColor color) {
    int i = 0;
    for (auto *m : Mushroom::mushrooms) {
        i += m->getColor() == color && m->getType() == Forge;
    }
    return i;
}

void Mushroom::paint() {
	SDL_RenderCopy(this->renderer, this->texture, NULL, this->rectTransform);
	this->bubble->paint();

	SDL_Rect *rect = new SDL_Rect({ -15, -15, 20, 20 });
	SDL_Rect *textPos = new SDL_Rect();
	*textPos = MathSDL::relativePosition(rect, this->rectTransform);
	this->unitText->WriteText(this->renderer, textPos);

	for (Unit *unit : this->units) {
		if (!unit->isArrived()) {
			unit->update();
		}
		else {
			unit->reinforceOrFight();
			this->units.erase(std::remove(this->units.begin(), this->units.end(), unit), this->units.end());
			// unit->erase();
		}
	}

	if (this->canLevelUp && this->color == Orange)
		this->levelUpButton->paint();

	if (canTransform(Forge) && this->color == Orange)
		this->forgeButton->paint();

	if (canTransform(Village) && this->color == Orange)
		this->villageButton->paint();
}

void Mushroom::update() {
    this->currentTime = SDL_GetTicks();
    this->countTime += this->currentTime - this->lastTime;

    if (this->countTime > Mushroom::UNIT_TICK && this->type == Village) {
        float increment = 1 + (this->level * 0.2f);

        if (this->nbUnits + increment <= Mushroom::MAX_UNIT * this->level) {
            this->nbUnits += increment;
        }

        if (this->nbUnits + increment > Mushroom::MAX_UNIT * this->level &&
            this->nbUnits < Mushroom::MAX_UNIT * this->level) {
            this->nbUnits = Mushroom::MAX_UNIT * this->level;
        }

        this->countTime -= Mushroom::UNIT_TICK;
    }

    if (this->color == Orange) {
        this->isLevelUpPossible();
    }

    this->lastTime = this->currentTime;
    this->unitText->setText(std::to_string((int) this->nbUnits));
}

void Mushroom::onDragBegin(MouseEvent event) {
    // TODO : UBISOFT, ÇA BUG ! (si on drag pas un orange)
    if (this->color == Orange) {
        this->startCursor->x = 0;
        this->startCursor->y = 0;

        this->endCursor->x = 0;
        this->endCursor->y = 0;

        this->direction->setStart(this->startCursor);
        this->direction->setEnd(this->endCursor);

        this->direction->setStartMushroom(this);
        this->direction->setActive(true);

        this->isDragging = true;
    }
}

void Mushroom::onDragEnd(MouseEvent event) {
	this->direction->setActive(false);
	this->ratio = ButtonUnitPercent::selectedButton->getPercent();

	if (this->isDragging) {
		for (auto &mushroom : Mushroom::mushrooms) {
			if (mushroom != this) {
				if (SDL_PointInRect(&event.cursor, mushroom->getRectTransform())) {
					this->direction->getStartMushroom()->sendUnits(mushroom, this->ratio);
				}
			}
		}
	}
}

void Mushroom::onDrag(MouseEvent event) {
    if (this->color == Orange) {
        this->startCursor->x = this->rectTransform->x + (this->rectTransform->w / 2);
        this->startCursor->y = this->rectTransform->y + (this->rectTransform->h / 2);

        this->endCursor->x = event.cursor.x;
        this->endCursor->y = event.cursor.y;

        this->direction->setStart(this->startCursor);
        this->direction->setEnd(this->endCursor);
    }
}

void Mushroom::sendUnits(Mushroom *end, int ratio) {
	SDL_SemWait(sem);
	ratio = MathSDL::clamp(ratio, 0, 100);

    float unitsToSend = this->nbUnits * float(ratio / 100.0f);
    unitsToSend = floor(unitsToSend);

	this->nbUnits -= unitsToSend;
	this->units.push_back(new Unit(this, end, (int)unitsToSend));
	SDL_SemPost(sem);
}

void Mushroom::checkDefeated(EMushroomColor color) {
    if (this->nbUnits <= 0) {
        // TODO Ok si orange attaquant => verifier aussi si vert attaque
        this->nbUnits = -this->nbUnits;
        this->color = color;
        (this->level == 1 ? this->level : this->level -= 1);
        this->generateImage();
    }
}

void Mushroom::generateImage() {
    std::string path = "";

    switch (this->color) {
        case Green:
            path += "green/mushroom-green-";
            break;
        case Orange:
            path += "orange/mushroom-orange-";
            break;
        case Grey:
            path += "grey/mushroom-grey-";
            break;
        default:
            this->imagePath = "";
    }

    if (path != "") {
        switch (this->type) {
            case Village:
                path += std::to_string(this->level);
                break;
            case Forge:
                path += "forge";
                break;
        }

        path += ".png";
    }

    this->imagePath = path;

    const char *imagePathChar = this->imagePath.c_str();

    this->renderer = GameLoop::Instance().getRenderer();
    this->image = AssetManager::LoadImage(imagePathChar);
    this->texture = SDL_CreateTextureFromSurface(this->renderer, this->image);

    this->rectTransform->x = this->x;
    this->rectTransform->y = this->y;
    this->rectTransform->w = this->image->w;
    this->rectTransform->h = this->image->h;

    MathSDL::scaleRectFromFHD(this->rectTransform);

    SDL_FreeSurface(this->image);
}

void Mushroom::setNbUnits(int units) {
	SDL_SemWait(sem);
	this->nbUnits = (float)units;
	SDL_SemPost(sem);
}

void Mushroom::levelUp() {
    if (!type == Village)
        return;

    if (this->level == 3 && this->nbUnits >= Mushroom::LEVELUPFROM3TO4) {
        this->level += 1;
        this->nbUnits -= Mushroom::LEVELUPFROM3TO4;
        this->generateImage();
    }

    if (this->level == 2 && this->nbUnits >= Mushroom::LEVELUPFROM2TO3) {
        this->level += 1;
        this->nbUnits -= Mushroom::LEVELUPFROM2TO3;
        this->generateImage();
    }

    if (this->level == 1 && this->nbUnits >= Mushroom::LEVELUPFROM1TO2) {
        this->level += 1;
        this->nbUnits -= Mushroom::LEVELUPFROM1TO2;
        this->generateImage();
    }
}

bool Mushroom::canTransform(EMushroomType type) {
    return this->type != type && this->nbUnits > TRANSFORMCOST;
}

void Mushroom::transform(EMushroomType type) {
    if (canTransform(type)) {
        this->level = 1;
        this->type = type;
        this->generateImage();
        this->nbUnits -= TRANSFORMCOST;
    }
    nbForge(Orange);
}

bool Mushroom::isLevelUpPossible() {
    this->canLevelUp = false;

    if (this->type == Village) {
        if (this->level == 3 && this->nbUnits >= Mushroom::LEVELUPFROM3TO4) {
            this->canLevelUp = true;
        } else if (this->level == 2 && this->nbUnits >= Mushroom::LEVELUPFROM2TO3) {
            this->canLevelUp = true;
        } else if (this->level == 1 && this->nbUnits >= Mushroom::LEVELUPFROM1TO2) {
            this->canLevelUp = true;
        }
    }
	return this->canLevelUp;
}

int Mushroom::getNbUnits() {

	SDL_SemWait(sem);
	int n =  (int)this->nbUnits;
	SDL_SemPost(sem);
	return n;
}

EMushroomColor Mushroom::getColor() {
    return this->color;
}

EMushroomType Mushroom::getType() {
    return this->type;
}