#include "headers/UIUnitCounter.h"

void UIUnitCounter::paint() {
    greenBar->paint();
    orangeBar->paint();

    textGreen->WriteText(renderer, textGreenRect);
    textOrange->WriteText(renderer, textOrangeRect);
}

void UIUnitCounter::update() {
    generateTarget();
}

void UIUnitCounter::awake() {
    GameObject::awake();
    orangeBar = new Image();
    orangeBar->setPath("red.png");
    orangeBar->awake();
    greenBar = new Image();
    greenBar->setPath("green.png");
    greenBar->awake();

    percent = 0.5;

    int midY = (rectTransform->h - 25) / 2;

    textOrangeRect = new SDL_Rect({ 60, midY, 25, 25 });
    *textOrangeRect = MathSDL::relativePosition(textOrangeRect, rectTransform);

    textGreenRect = new SDL_Rect({ getRectTransform()->x - 20, midY, 25, 25 });
    *textGreenRect = MathSDL::relativePosition(textGreenRect, rectTransform);

    targetOrange = new SDL_Rect({
            this->rectTransform->x,
            this->rectTransform->y,
            this->rectTransform->w = (int) ceil(40 + this->rectTransform->w * percent),
            this->rectTransform->h
    });

    textGreen = new Text("obelixpro/ObelixPro-cyr.ttf", 12, {255, 255, 255, 255});
    textOrange = new Text("obelixpro/ObelixPro-cyr.ttf", 12, {255, 255, 255, 255});
    textOrange->OpenFont();
    textGreen->OpenFont();

    greenBar->setRectTransform(new SDL_Rect({
            this->rectTransform->x,
            this->rectTransform->y,
            this->rectTransform->w,
            this->rectTransform->h
    }));
    tweenOrange = new Tween(orangeBar->getRectTransform(), targetOrange, 200);
}

void UIUnitCounter::generateTarget() {
    int orange = 0, green = 0;

    for (auto mushroom : Mushroom::mushrooms) {
        switch (mushroom->getColor()) {
            case Green:
                green += mushroom->getNbUnits();
                break;
            case Orange:
                orange += mushroom->getNbUnits();
                break;
        }
    }

    textOrange->setText(std::to_string(orange));
    textGreen->setText(std::to_string(green));

    float percent = 0.5;

    if (orange != green) {
        percent = orange / float(orange + green);
    }

    this->percent = percent;

    int size = this->rectTransform->w - 80;

    this->targetOrange->w = (int) ceil(40 + size * percent);
}