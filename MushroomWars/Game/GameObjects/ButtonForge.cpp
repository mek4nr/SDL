#include "headers/ButtonForge.h"

ButtonForge::ButtonForge(Mushroom *mushroom) : target(mushroom) {
    Button::Button();
    this->setImageNormal("upgrade-forge.png");
    this->setImageHover("upgrade-forge-hover.png");
}

void ButtonForge::onPointerDown(MouseEvent event) {
    this->target->transform(Forge);
}