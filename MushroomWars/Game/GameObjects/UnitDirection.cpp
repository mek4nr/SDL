#include "headers/UnitDirection.h"

void UnitDirection::setStart(SDL_Point *start) {
    this->startPoint = start;
}

void UnitDirection::setEnd(SDL_Point *end) {
    this->end = end;
}

void UnitDirection::paint() {
    SDL_SetRenderDrawColor(this->renderer, 255, 255, 255, 255);
    SDL_RenderDrawLine(
            this->renderer,
            this->startPoint->x,
            this->startPoint->y,
            this->end->x,
            this->end->y
    );
}

void UnitDirection::update() {
    super::update();
}

void UnitDirection::setStartMushroom(Mushroom *mushroom) {
    this->startMushroom = mushroom;
}

Mushroom *UnitDirection::getStartMushroom() {
    return this->startMushroom;
}

