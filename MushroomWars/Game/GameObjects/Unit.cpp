#include "../../../Utils/MathSDL.h"
#include "headers/Unit.h"
#include "../Level1/Level1AI.h"

Unit::Unit(Mushroom *start, Mushroom *end, int power) : start(start), end(end), power(power) {
    this->rectTransform->x = start->getRectTransform()->x + (this->getRectTransform()->w / 2);
    this->rectTransform->y = start->getRectTransform()->y + (this->getRectTransform()->h / 2);

    this->rectTransform->w = MathSDL::scaleFromFHD(40);
    this->rectTransform->h = this->rectTransform->w;
	this->colorStart = start->getColor();

    this->image = new Image();

	switch (start->getColor())
	{
	case Orange:
		this->image->setPath("orange/mushroom-orange.png");
		break;
	case Green:
		this->image->setPath("green/mushroom-green.png");
		break;
	}

    
    this->image->awake();

    this->lastTime = SDL_GetTicks();
	this->checkpoints = Level1AI::Instance().GetCheckpoints(
		new SDL_Point{ this->start->getRectTransform()->x, this->start->getRectTransform()->y }, 
		new SDL_Point{ this->end->getRectTransform()->x, this->end->getRectTransform()->y }
	);
}

void Unit::move() {
    if (this->needPath) {
        this->calculatePath();
        this->needPath = false;
    }

    this->rectTransform->x = !this->goBackward ? this->rectTransform->x += 2 : this->rectTransform->x -= 2;
    this->rectTransform->y = (int)(this->rectTransform->x * this->slope + this->intersect);

    if (this->nextCheckpoint + 1 <= this->checkpoints.size()) {
        if (SDL_PointInRect(this->checkpoints.at(this->nextCheckpoint), this->rectTransform)) {
            this->nextCheckpoint += 1;
            this->needPath = true;
        }
    }
}

void Unit::calculatePath() {
    SDL_Point p1;
    SDL_Point p2;

    this->goBackward = false;

    if (this->nextCheckpoint <= 0) {
        p1 = { this->start->getRectTransform()->x, this->start->getRectTransform()->y };
        this->centerStartPoint(&p1);
    } else {
        p1 = { this->checkpoints.at(this->nextCheckpoint - 1)->x, this->checkpoints.at(this->nextCheckpoint - 1)->y };
    }

    if (this->nextCheckpoint >= this->checkpoints.size()) {
        p2 = { this->end->getRectTransform()->x, this->end->getRectTransform()->y };
        this->centerEndPoint(&p2);
    } else {
        p2 = { this->checkpoints.at(this->nextCheckpoint)->x, this->checkpoints.at(this->nextCheckpoint)->y };
    }

    MathSDL::getEquationBetweenTwoPoints(&p1, &p2, &this->slope, &this->intersect);

    std::cout << "P1 : [ " + std::to_string(p1.x) + " ; " + std::to_string(p1.y) + " ]" << std::endl;
    std::cout << "P2 : [ " + std::to_string(p2.x) + " ; " + std::to_string(p2.y) + " ]" << std::endl;

    std::cout << "y = " + std::to_string(this->slope) + "x + " + std::to_string(this->intersect) << std::endl;

    if ((&p2)->x - (&p1)->x < 0 && (&p2)->y - (&p1)->y) {
        this->goBackward = true;
    }
}

void Unit::reinforceOrFight() {
    this->end->getColor() == colorStart ? this->reinforce() : this->fight();
}

void Unit::reinforce() {
    this->end->setNbUnits(this->end->getNbUnits() + this->power);
}

void Unit::fight() {
	// Changer le set pas un + ou - sinon ca peut cr�er des problemes lorsque 2 unit� arrive pile au meme moment
    this->end->setNbUnits(
		this->end->getNbUnits() / (1+Mushroom::nbForge(this->start->getColor())*0.2) - 
		this->power / (1 + Mushroom::nbForge(this->end->getColor())*0.2));
    this->end->checkDefeated(this->start->getColor());
}

void Unit::centerStartPoint(SDL_Point *p) {
    p->x = p->x + (this->start->getRectTransform()->w - this->rectTransform->w) / 2;
    p->y = p->y + (this->start->getRectTransform()->h - this->rectTransform->h) / 2;
}

void Unit::centerEndPoint(SDL_Point *p) {
    p->x = p->x + (this->end->getRectTransform()->w - this->rectTransform->w) / 2;
    p->y = p->y + (this->end->getRectTransform()->h - this->rectTransform->h) / 2;
}

void Unit::update() {
    this->currentTime = SDL_GetTicks();
    this->countTime += this->currentTime - this->lastTime;

    if (this->countTime > Unit::MOVE_TICK) {
        this->move();
        this->countTime -= Unit::MOVE_TICK;
    }

    this->lastTime = this->currentTime;

    this->paint();
}

void Unit::paint() {
    this->image->setRectTransform(this->rectTransform);
    this->image->paint();
}

void Unit::addCheckpoint(SDL_Point *point) {
    this->checkpoints.push_back(point);
}

void Unit::setCheckpoints(std::vector<SDL_Point *> points) {
    this->checkpoints = points;
}

bool Unit::isArrived() {
    return SDL_HasIntersection(this->rectTransform, this->end->getRectTransform());
}
