#ifndef MUSHROOM_WARS_MUSHROOM_H
#define MUSHROOM_WARS_MUSHROOM_H

enum EMushroomType {
	Village,
	Forge
};

enum EMushroomColor {
	Orange = 1,
	Green = 2,
	Grey = 3
};

#include "../../../../Include.h"
#include "../../../../Engine/headers/GameObject.h"
#include "../../../../Engine/headers/AssetManager.h"
#include "../../../../Engine/UI/headers/Image.h"
#include "../../../../Utils/MathSDL.h"
#include "UnitDirection.h"
#include "Unit.h"
#include "../../../../Engine/UI/headers/Text.h"
#include "ButtonUpgrade.h"
#include "ButtonVillage.h"
#include "ButtonForge.h"

class Unit;
class UnitDirection;
class ButtonUpgrade;
class ButtonForge;
class ButtonVillage;



class Mushroom : public GameObject, public IPointerDown, public IPointerDrag {
public:
    typedef GameObject super;

    Mushroom(EMushroomColor color, int x, int y, int level, int nbUnit, EMushroomType type, UnitDirection *direction);

    static const int MAX_UNIT = 20;
    static const int TRANSFORMCOST = 5;

    static const int LEVELUPFROM1TO2 = 5;
    static const int LEVELUPFROM2TO3 = 10;
    static const int LEVELUPFROM3TO4 = 20;

    static const int UNIT_TICK = 1000;
    static std::vector<Mushroom *> mushrooms;

    static int nbForge(EMushroomColor color);

    std::vector<Unit *> units;

    Image *bubble;
    ButtonUpgrade *levelUpButton;
    ButtonForge *forgeButton;
    ButtonVillage *villageButton;

    void paint() override;

    void update() override;

    void awake() override;

    void onDragBegin(MouseEvent event) override;

    void onDragEnd(MouseEvent event) override;

    void onDrag(MouseEvent event) override;

    void generateImage();

    void setNbUnits(int units);

	void sendUnits(Mushroom *end, int ratio);

    void checkDefeated(EMushroomColor color);

    void levelUp();

    bool isLevelUpPossible();

    bool canTransform(EMushroomType type);

    void transform(EMushroomType type);

    int getNbUnits();

    EMushroomType getType();

    EMushroomColor getColor();

protected:
    EMushroomColor color;
    EMushroomType type;
    int x, y;
    float nbUnits = 0.0f;

	SDL_sem *sem;

	SDL_Surface *image;

private:
    int level;
    int ratio = 0;

    unsigned long currentTime = 0;
    unsigned long countTime = 0;
    unsigned long lastTime = 0;

    bool canLevelUp;
    bool isDragging = false;
    std::string imagePath;

    Text *unitText;
    UnitDirection *direction = nullptr;

    SDL_Texture *texture;
    SDL_Point *startCursor = new SDL_Point();
    SDL_Point *endCursor = new SDL_Point();
};

#endif //MUSHROOM_WARS_MUSHROOM_H
