#ifndef MUSHROOM_WARS_BUTTONVILLAGE_H
#define MUSHROOM_WARS_BUTTONVILLAGE_H

#include "../../../../Include.h"
#include "../../GameObjects/headers/Mushroom.h"
#include "../../../../Engine/UI/headers/Button.h"

class ButtonVillage : public Button
{
private:
	Mushroom* target;
public:
	ButtonVillage(Mushroom* mushroom);
	void onPointerDown(MouseEvent event) override;
};

#endif