#ifndef MUSHROOMWAR_BUTTONMAINMENU_H
#define MUSHROOMWAR_BUTTONMAINMENU_H

#include "../../../../Include.h"
#include "../../../../Engine/UI/headers/Button.h"
#include "../../../MainMenu/Scene/MainMenuScene.h"

class ButtonMainMenu : public Button
{
public:
	ButtonMainMenu();
	void onPointerDown(MouseEvent event) override;
};

#endif //MUSHROOMWAR_BUTTONMAINMENU_H
