#ifndef MUSHROOM_WARS_UNITDIRECTION_H
#define MUSHROOM_WARS_UNITDIRECTION_H

#include "../../../../Engine/headers/GameObject.h"
#include "Mushroom.h"

class Mushroom;

class UnitDirection : public GameObject {
public:
	typedef GameObject super;

	void setStart(SDL_Point *start);

	void setEnd(SDL_Point *end);

    void setStartMushroom(Mushroom *mushroom);

    Mushroom *getStartMushroom();

    void paint() override;

	void update() override;

private:
    SDL_Point *startPoint;
    SDL_Point *end;

    Mushroom *startMushroom;
};

#endif //MUSHROOM_WARS_UNITDIRECTION_H
