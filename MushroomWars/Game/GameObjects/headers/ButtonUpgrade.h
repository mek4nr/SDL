#ifndef MUSHROOM_WARS_BUTTONUPGRADE_H
#define MUSHROOM_WARS_BUTTONUPGRADE_H

#include "../../../../Include.h"
#include "../../GameObjects/headers/Mushroom.h"
#include "../../../../Engine/UI/headers/Button.h"

class ButtonUpgrade : public Button
{
private:
	Mushroom* target;
public:
	ButtonUpgrade(Mushroom* mushroom);
	void onPointerDown(MouseEvent event) override;
};

#endif