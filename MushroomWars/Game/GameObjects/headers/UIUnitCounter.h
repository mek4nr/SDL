#ifndef MUSHROOMWAR_UIUNITCOUNTER_H
#define MUSHROOMWAR_UIUNITCOUNTER_H

#include "../../../../Include.h"
#include "../../../../Engine/headers/GameObject.h"
#include "../../../../Engine/UI/headers/Image.h"
#include "../../../../Engine/Tween/Tween.h"
#include "../../../../Engine/UI/headers/Text.h"
#include "Mushroom.h"

class UIUnitCounter : public GameObject
{
private:
	Image* orangeBar;
	Image* greenBar;
	Text* textOrange;
	SDL_Rect* textOrangeRect;
	Text* textGreen;
	SDL_Rect* textGreenRect;
	SDL_Rect* targetOrange;
	Tween* tweenOrange;
	
	float percent = 0;
public:
	void paint() override;
	void update() override;
	void awake() override;
	void generateTarget();
};

#endif //MUSHROOMWAR_UIUNITCOUNTER_H
