#ifndef MUSHROOMWAR_GRIDMAP_H
#define MUSHROOMWAR_GRIDMAP_H

#include "../../../../Include.h"
#include "../../../../Engine/UI/headers/Image.h"
#include "../../../../Utils/MathSDL.h"
#include "Mushroom.h"
#include "../../../AI/MushroomPath.h"

class GridMap : public Image, public IPointerDown
{
private:
    SDL_Point* startPoint;
    SDL_Point* endPoint;
    bool start = true;
    Image* red;
    Image* green;
    Text* orderMove;
    std::vector<SDL_Point*> move;

    SDL_Point* avoidPoint[8] = {
            //Left river
            MathSDL::factorPoint({0.25f, 0.49f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            MathSDL::factorPoint({0.33f, 0.42f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            //Right river
            MathSDL::factorPoint({0.67f, 0.41f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            MathSDL::factorPoint({0.75f, 0.48f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            //Left rift
            MathSDL::factorPoint({0.20f, 0.12f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            MathSDL::factorPoint({0.30f, 0.09f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            //Right rift
            MathSDL::factorPoint({0.65f, 0.08f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            MathSDL::factorPoint({0.77f, 0.11f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
    };

    SDL_Point* line[8] = {
            //Left river
            MathSDL::factorPoint({0.07f, 0.08f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            MathSDL::factorPoint({0.47f, 0.95f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            //Right river
            MathSDL::factorPoint({0.89f, 0.18f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            MathSDL::factorPoint({0.55f, 0.93f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            //Rift Rect
            MathSDL::factorPoint({0.34f, 0.03f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            MathSDL::factorPoint({0.64f, 0.35f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            //Rift scalaire
            MathSDL::factorPoint({0.50f, 0.03f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
            MathSDL::factorPoint({0.51f, 0.30f}, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()),
    };

    MushroomPath mushroomPath;

public:
    GridMap(std::string path);
    void onPointerDown(MouseEvent event) override;
    void paint() override ;
};


#endif //MUSHROOMWAR_GRIDMAP_H
