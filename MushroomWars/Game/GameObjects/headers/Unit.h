#ifndef MUSHROOM_WARS_UNIT_H
#define MUSHROOM_WARS_UNIT_H

#include "Mushroom.h"
#include "../../../../Engine/UI/headers/Image.h"

class Mushroom;
class Text;

class Unit : public GameObject {
public:
    typedef GameObject super;

    Unit(Mushroom *start, Mushroom *end, int power);

    static const int MOVE_TICK = 20;

    float slope, intersect;

    bool goBackward = false;

    Image *image;

    void paint() override;

    void update() override;

    void centerStartPoint(SDL_Point *p);

    void centerEndPoint(SDL_Point *p);

    void calculatePath();

    void reinforceOrFight();

    void reinforce();

    void fight();

    void addCheckpoint(SDL_Point *point);

    void setCheckpoints(std::vector<SDL_Point *> points);

    bool isArrived();

private:
    Mushroom *start;
    Mushroom *end;
	EMushroomColor colorStart;

    unsigned int nextCheckpoint = 0;
    int power;

    bool needPath = true;

    unsigned long currentTime = 0;
    unsigned long countTime = 0;
    unsigned long lastTime = 0;

    std::vector<SDL_Point *> checkpoints;

    void move();
};


#endif //MUSHROOM_WARS_UNIT_H
