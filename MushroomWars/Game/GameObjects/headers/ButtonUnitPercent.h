#ifndef MUSHROOM_WARS_BUTTONUNITPERCENT_H
#define MUSHROOM_WARS_BUTTONUNITPERCENT_H

#include "../../../../Engine/UI/headers/Button.h"

class ButtonUnitPercent : public Button {
public:
    typedef Button super;

    ButtonUnitPercent(int percent);

    static const int PERCENT100 = 100;
    static const int PERCENT75 = 75;
    static const int PERCENT50 = 50;
    static const int PERCENT25 = 25;

    static ButtonUnitPercent *selectedButton;

    void onPointerDown(MouseEvent event) override;

    void setSelected(bool selected) override;

    int getPercent();

private:
    int percent;
};


#endif //MUSHROOM_WARS_BUTTONUNITPERCENT_H
