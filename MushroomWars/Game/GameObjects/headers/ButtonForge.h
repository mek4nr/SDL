#ifndef MUSHROOM_WARS_BUTTONFORGE_H
#define MUSHROOM_WARS_BUTTONFORGE_H

#include "../../../../Include.h"
#include "../../GameObjects/headers/Mushroom.h"
#include "../../../../Engine/UI/headers/Button.h"

class ButtonForge : public Button
{
private:
	Mushroom* target;
public:
	ButtonForge(Mushroom* mushroom);
	void onPointerDown(MouseEvent event) override;
};

#endif