#include "headers/ButtonUpgrade.h"

ButtonUpgrade::ButtonUpgrade(Mushroom *mushroom) : target(mushroom) {
    Button::Button();
    this->setImageNormal("upgrade-arrow.png");
    this->setImageHover("upgrade-arrow-hover.png");
}

void ButtonUpgrade::onPointerDown(MouseEvent event) {
    this->target->levelUp();
}