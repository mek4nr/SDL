#include "headers/GridMap.h"
#include "../../../Utils/MathSDL.h"

void GridMap::onPointerDown(MouseEvent event) {
    float x = event.cursor.x/(float)GameLoop::Instance().getWidth(), y = event.cursor.y/(float)GameLoop::Instance().getHeight();

//    GameLoop::Log("point :" + std::to_string(x) + "," + std::to_string(y));
//
//    GameLoop::Log("point :" + std::to_string(event.cursor.x) + "," + std::to_string(event.cursor.y));
    if(start)
    {
        startPoint = new SDL_Point{event.cursor.x, event.cursor.y};
    }
    else
    {
        endPoint = new SDL_Point{event.cursor.x, event.cursor.y};
        move.clear();
        move = mushroomPath.getMoves(startPoint, endPoint);
        GameLoop::Log("///");
    }
    start = !start;

}

GridMap::GridMap(std::string path) {
    std::vector<Zone*> zones;

    zones.push_back(new PathLine(line[0], MathSDL::vector(line[0], line[1]), avoidPoint[0], avoidPoint[1]));
    zones.push_back(new PathLine(line[2], MathSDL::vector(line[2], line[3]), avoidPoint[2], avoidPoint[3]));
    zones.push_back(new RectZoneWith2SideEntry(
            MathSDL::createRect(line[4], line[5]),
            line[7],
            MathSDL::vector(line[7], line[6]),
            AvoidingZone{avoidPoint[4], avoidPoint[5]},
            AvoidingZone{avoidPoint[7], avoidPoint[6]}
    ));
    mushroomPath = MushroomPath();
    mushroomPath.zones = zones;

    this->path = path;
    this->setPath(path);
    this->awake();
    this->rectTransform = new SDL_Rect{0,0,GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight()};
    red = new Image();
    red->setPath("red.png");
    red->awake();
    green = new Image();
    green->setPath("green.png");
    green->awake();
    orderMove = new Text("obelixpro/ObelixPro-cyr.ttf", 12, { 255, 255, 255, 255 });
}

void GridMap::paint() {
    Image::paint();

    for(auto* p : avoidPoint)
    {
        red->setRectTransform(new SDL_Rect{p->x, p->y, 10,10});
        red->paint();
    }

    for(auto* p : line)
    {
        green->setRectTransform(new SDL_Rect{p->x, p->y, 10,10});
        green->paint();
    }

    int cmp = 0;
    for(auto* p : move)
    {
        ++cmp;
        orderMove->setText(std::to_string(cmp));
        orderMove->WriteText(renderer, new SDL_Rect{p->x, p->y, 20,20});
    }
}
