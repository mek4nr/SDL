#include "headers/ButtonVillage.h"

ButtonVillage::ButtonVillage(Mushroom *mushroom) : target(mushroom) {
    Button::Button();
    this->setImageNormal("upgrade-village.png");
    this->setImageHover("upgrade-village-hover.png");
}

void ButtonVillage::onPointerDown(MouseEvent event) {
    this->target->transform(Village);
}