#include "headers/ButtonMainMenu.h"

ButtonMainMenu::ButtonMainMenu()
{
	this->setText("Home");
	this->setTextNormal(new Text("obelixpro/ObelixPro-cyr.ttf", 12, { 255, 255, 255, 255 }));
	this->setTextHover(new Text("obelixpro/ObelixPro-cyr.ttf", 12, { 0, 0, 0, 255 }));
	this->setImageNormal("backgroundButtonMenu.png");
	this->setRectText(new SDL_Rect{ 20,20,20,20 });
	auto* rMenu = new SDL_Rect{ 100,0,10,10 };
	MathSDL::percentRect(rMenu, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight(), TopRight);
	this->setRectTransform(rMenu);
}

void ButtonMainMenu::onPointerDown(MouseEvent event)
{
	MainMenuScene *game = new MainMenuScene();
	GameLoop::Instance().loadScene(game);
}