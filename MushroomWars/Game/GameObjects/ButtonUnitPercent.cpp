#include "headers/ButtonUnitPercent.h"

ButtonUnitPercent *ButtonUnitPercent::selectedButton = nullptr;

ButtonUnitPercent::ButtonUnitPercent(int percent) {

    this->percent = percent;

    this->setText(std::to_string(this->percent));

    this->setTextNormal(new Text("obelixpro/ObelixPro-cyr.ttf", 12, {255, 255, 255, 255}));
    this->setImageNormal("percents/percent.png");
    this->setImageSelected("percents/percent-selected.png", new SDL_Color());

    this->setTextHover(new Text("obelixpro/ObelixPro-cyr.ttf", 12, {0, 0, 0, 255}));
    this->setRectText(new SDL_Rect{0, 0, 0, 0});

    auto *r = new SDL_Rect({0, 0, 10, 10});

    switch (this->percent) {
        case ButtonUnitPercent::PERCENT100:
            r = new SDL_Rect({20, 380, 70, 70});
            break;
        case ButtonUnitPercent::PERCENT75:
            r = new SDL_Rect({20, 486, 70, 70});
            break;
        case ButtonUnitPercent::PERCENT50:
            r = new SDL_Rect({20, 594, 70, 70});
            break;
        case ButtonUnitPercent::PERCENT25:
            r = new SDL_Rect({20, 702, 70, 70});
            break;
    }

    MathSDL::scaleRectFromFHD(r);
    this->setRectTransform(r);
}

void ButtonUnitPercent::onPointerDown(MouseEvent event) {
    this->setSelected(true);
}

void ButtonUnitPercent::setSelected(bool selected) {
    super::setSelected(selected);

    if (selected) {
        if (ButtonUnitPercent::selectedButton != nullptr && ButtonUnitPercent::selectedButton != this) {
            ButtonUnitPercent::selectedButton->setSelected(false);
        }

        ButtonUnitPercent::selectedButton = this;
    }
}

int ButtonUnitPercent::getPercent() {
    return this->percent;
}
