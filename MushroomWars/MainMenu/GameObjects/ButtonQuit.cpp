#include "ButtonQuit.h"

ButtonQuit::ButtonQuit()
{
	this->setText("Quit");
	this->setTextNormal(new Text("obelixpro/ObelixPro-cyr.ttf", 12, { 255, 255, 255, 255 }));
	this->setTextHover(new Text("obelixpro/ObelixPro-cyr.ttf", 12, { 0, 0, 0, 255 }));
	this->setImageNormal("backgroundButtonMenu.png");
	this->setRectText(new SDL_Rect{ 20,20,20,20 });
	auto* rQuit = new SDL_Rect{ 50,50,10,10 };
	MathSDL::percentRect(rQuit, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight(), Center);
	this->setRectTransform(rQuit);
}

void ButtonQuit::onPointerDown(MouseEvent event)
{
	GameLoop::Instance().exit();
}