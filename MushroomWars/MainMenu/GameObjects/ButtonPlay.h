#ifndef MUSHROOMWAR_BUTTONPLAY_H
#define MUSHROOMWAR_BUTTONPLAY_H

#include "../../../Engine/UI/headers/Button.h"
#include "../../Game/Scenes/GameScene.h"

class ButtonPlay : public Button
{
public:
	ButtonPlay();
	void onPointerDown(MouseEvent event) override;
};

#endif //MUSHROOMWAR_BUTTONPLAY_H
