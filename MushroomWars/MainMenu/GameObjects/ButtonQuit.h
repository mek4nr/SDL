#ifndef MUSHROOMWAR_BUTTONQUIT_H
#define MUSHROOMWAR_BUTTONQUIT_H

#include "../../../Engine/UI/headers/Button.h"

class ButtonQuit : public Button
{
public:
	ButtonQuit();
	void onPointerDown(MouseEvent event) override;
};

#endif //MUSHROOMWAR_BUTTONQUIT_H
