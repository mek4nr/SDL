#include "ButtonPlay.h"

ButtonPlay::ButtonPlay()
{
	this->setText("play");
	this->setTextNormal(new Text("obelixpro/ObelixPro-cyr.ttf", 12, { 255, 255, 255, 255 }));
	this->setTextHover(new Text("obelixpro/ObelixPro-cyr.ttf", 12, { 0, 0, 0, 255 }));
	this->setImageNormal("backgroundButtonMenu.png");
	this->setRectText(new SDL_Rect{ 20,20,20,20 });
	auto* rPlay = new SDL_Rect{ 50,30,10,10 };
	MathSDL::percentRect(rPlay, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight(), Center);
	this->setRectTransform(rPlay);
}

void ButtonPlay::onPointerDown(MouseEvent event)
{
	auto *game = new GameScene();
	GameLoop::Instance().loadScene(game);
}