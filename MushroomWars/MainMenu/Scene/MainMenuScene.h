#ifndef MUSHROOMWAR_MAINMENUSCENE_H
#define MUSHROOMWAR_MAINMENUSCENE_H

#include "../../../Include.h"
#include "../../../Engine/headers/Scene.h"
#include "../../../Engine/UI/headers/Image.h"
#include "../../../Engine/headers/AssetManager.h"
#include "../GameObjects/ButtonPlay.h"
#include "../GameObjects/ButtonQuit.h"
#include "../../../Engine/UI/headers/AnimatedSprite.h"

class ButtonPlay;
class ButtonQuit;
class Image;

class MainMenuScene : public Scene
{
private:
	ButtonPlay* play;
	ButtonQuit* quit;
	Image* background = new Image();
public:
	typedef Scene super;
	void init() override;
};

#endif
