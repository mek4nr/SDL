#include "MainMenuScene.h"

void MainMenuScene::init()
{
	super::init();

	background->setPath("mainbackground.jpg");
	background->setRectTransform(NULL);
	background->setScene(this);

	AnimatedSprite* sprite = new AnimatedSprite();
	auto* rGoomba = new SDL_Rect{ 50,85,10,17 };
    MathSDL::percentRect(rGoomba, GameLoop::Instance().getWidth(), GameLoop::Instance().getHeight(), Center);
	sprite->setRectTransform(rGoomba);
	sprite->setScene(this);
	sprite->setTimePerFrame(100);

	for (int i = 0; i <= 4; i++)
	{
		std::string t = "goomba/front/" + std::to_string(i) + ".png";
		sprite->addImage(t);
	}
	for (int i = 3; i > 0; i--)
	{
		std::string t = "goomba/front/" + std::to_string(i) + ".png";
		sprite->addImage(t);
	}

	play = new ButtonPlay();
	play->setScene(this);

	quit = new ButtonQuit();
	quit->setScene(this);
}