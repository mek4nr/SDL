#ifndef MUSHROOMWAR_INCLUDE_H
#define MUSHROOMWAR_INCLUDE_H

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 800

#include <SDL.h>
#include <SDL_surface.h>
#include <SDL_rect.h>
#include <SDL_system.h>
#include <SDL_mouse.h>
#include <SDL_events.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#endif //MUSHROOMWAR_INCLUDE_H
