#include "Include.h"
#include "Engine/headers/GameLoop.h"
#include "MushroomWars/Game/Scenes/GameScene.h"
#include "MushroomWars/MainMenu/Scene/MainMenuScene.h"
#include "MushroomWars/Game/Scenes/TestScene.h"

using namespace std;

void startGame() {
	//GameScene *game = new GameScene();
	//TestScene *game = new TestScene();
	MainMenuScene *game = new MainMenuScene();
	GameLoop::Instance().init("Mushroom Wars", SCREEN_WIDTH, SCREEN_HEIGHT, game);
}

int main(int argc, char *argv[]) {
	startGame();

	std::cerr << "Last Error" << std::endl;
	std::cerr << SDL_GetError() << std::endl;
	std::cerr << IMG_GetError() << std::endl;
	std::cerr << TTF_GetError() << std::endl;

	return 0;
}